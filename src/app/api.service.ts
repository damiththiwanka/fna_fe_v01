import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { cd_family_details } from "./modules/cd_familydetails";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private httpClient: HttpClient) { }

  API_KEY = 'bf80fb39094340e4a012c879a0bfa1b3';

  getNews() {
    return this.httpClient.get(`https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=${this.API_KEY}`);
  }

  AAA_login() {
    var param = {
      "username": "scott",
      "password": "tiger",
      "device_id": "123456",
      "unique_id": "123456"
    }

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.httpClient.post(
      `https://aaa.allianz.lk/frontend/web/index.php?r=user/getarenamobile`,
      param,
      httpOptions
    );
  }

  save_customer_info(){
    
  }
  getAusysToken(){
    // var param = new HttpParams().set("grant_type=client_credentials");
    
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':'Basic ZXByb3AtY2xpZW50OmVwcm9wLXNlY3JldA=='
      })
    };
    //http://10.71.64.76:8081/oauth/token
    //http://DESKTOP-2VIIB4D:8081/oauth/token
    return this.httpClient.post(
      `http://DESKTOP-2VIIB4D.allianzlk.int:8081/oauth/token`,
      "grant_type=client_credentials",
      httpOptions
    );
  }
  save_CD_expence_info(details:cd_family_details){

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }



}





