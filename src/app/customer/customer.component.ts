import { Component, OnInit } from '@angular/core';
import { formatCurrency } from "@angular/common";
import { CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { max } from 'rxjs/operators';
import { SourceListMap } from 'source-list-map';
import { Chart } from "chart.js";
import { Color } from 'ng2-charts';
import { MergeMapSubscriber } from 'rxjs/internal/operators/mergeMap';
import { ApiService } from "../api.service";
import { cd_token_ausys } from "../modules/token_ausys"
import { aaa_token } from '../modules/aaa_token';

import { ActivatedRoute } from '@angular/router';



export interface Ocupation {
  value: string;
  viewValue: string;
}
export interface Member {
  title: string;
  image: string;
}


@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {
  // tslint:disable-next-line:variable-name

  public category: any;
  public categoryType: any;

  constructor(
    private _formBuilder: FormBuilder,
    private apiService: ApiService,
    private route: ActivatedRoute
  ) { }

  public labels = ['50000', '75000', '100000', '200000'];
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  forthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  selectedValue: string;
  selectedCar: string;

  amount_father: number = 0;
  amount_mother: number = 0;
  age_father: number = 25;
  age_mother: number = 25;
  age_boy01: number = 12;
  age_girl01: number = 12;
  father: boolean = false;
  mother: boolean = false;
  boy01: boolean = false;
  girl01: boolean = false;

  hotel_expenses: number = 0;
  Critical_Illness: number = 0;
  Disability_expenses: number = 0;
  Retirement_fund: number = 0;
  Life_Insurance: number = 0;

  panelOpenState = false;

  tokenesponce:cd_token_ausys;

  Familyfather: Member = { title: 'Father', image: 'assets/img/main.png' }
  Familymother: Member = { title: 'Mother', image: 'assets/img/wife.png' }
  Familyboy01: Member = { title: 'Boy 01', image: 'assets/img/child_m.png' }
  Familygirl01: Member = { title: 'Girl 01', image: 'assets/img/child_f.png' }

  family: Member[] = [
    this.Familyfather,
    this.Familymother,
    this.Familyboy01,
    this.Familygirl01
    // ,
    // {
    //   title: 'Boy 02',
    //   image: 'assets/img/child_m.png'
    // },
    // {
    //   title: 'Girl 02',
    //   image: 'assets/img/child_f.png'
    // }
    // ,
    // {
    //   title: 'Boy 03',
    //   image: 'assets/img/child_m.png'
    // },
    // {
    //   title: 'Girl 03',
    //   image: 'assets/img/child_f.png'
    // }
  ];

  //080610 token number

  done: Member[] = [

  ];

  ocupation: Ocupation[] = [
    { value: 'SE', viewValue: 'Software Engineer' },
    { value: 'ME', viewValue: 'Mechanical Engineer' },
    { value: 'EE', viewValue: 'Engineer' }
  ];

  // Financial Strenth ++++++++

  Cash_Other_assets: number = 0;
  Real_Estate_Resident: number = 0;
  Real_Estate_No_Resident: number = 0;
  Business_Interest: number = 0;
  Pension_Fund: number = 0;
  Insurance_cash_value: number = 0;

  Mortgage_Residence: number = 0;
  Mortgage_Others: number = 0;
  Auto_Loan: number = 0;
  Credit_Card: number = 0;
  Debts: number = 0;
  Other_debt_obligations: number = 0;

  MFS_Assets: number = 0;
  MFS_Liabilities: number = 0;
  MFS_Balance: number = 0;


  // CHART ++++++++

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
  };

  public mbarChartLabels: string[] = ['Assets', 'liabilities'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = false;

  public barChartColors: Array<any> = [
    {
      backgroundColor: 'rgba(105,159,177,0.2)',
      borderColor: 'rgba(105,159,177,1)',
      pointBackgroundColor: 'rgba(105,159,177,1)',
      pointBorderColor: '#fafafa',
      pointHoverBackgroundColor: '#fafafa',
      pointHoverBorderColor: 'rgba(105,159,177)'
    },
    {
      backgroundColor: 'rgba(77,20,96,0.3)',
      borderColor: 'rgba(77,20,96,1)',
      pointBackgroundColor: 'rgba(77,20,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,20,96,1)'
    }
  ];
  public barChartData: any[] = [
    { data: [0, 0] }
  ];
  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  //Chart --------
  // Financial Strenth ----------

  //Cash & Income Need ++++++++++
  Immediate_Money_Fund: number = 0;
  Immediate_Money_Fund_times: number = 6;

  Debt_liquidation: number = 0;
  Debt_liquidation_times: number = 6;

  Emergency_Fund: number = 0;
  Emergency_Fund_times: number = 3;

  Educational_Fund: number = 10;

  Mortgage_Or_Rent: number = 0;

  rentOption = false;
  CD_rent_amount: number = 5000;


  color = 'primary';

  IncomeObjective: number = 70;
  AnnualGuaranteedincomes: number = 0;
  AssumeInterest: number = 5;
  OtherAnnualincomes: number = 0;
  IncomeNeed: number = 0;
  IncomeNeedString: String;
  Cashneed: number = 0;
  CashNeedString: String;
  IncomeNeedTemp: number = 0;
  ProtectFamilyFinancial: number = 0;
  ProtectFamilyFinancialString: String = "";

  //Cash & Income Need ----------


  // Retirement & Investment +++++++++++
  FocusAnnualIncomeatRetirement: number = 0;
  FocusAnnualIncomeatRetirementString: String = "";

  Assumeinterestrate: number = 5;

  Capitalneeded: number = 0;
  CapitalneededString: String = "";


  Childreneducationfund: number = 2000000;
  ChildreneducationfundString: String = "";

  OtherSavings: number = 0;
  OtherSavingsString: String = "";

  InvestmentLumpSum: number = 0;
  InvestmentLumpSumString: String = "";

  Age: number = 25;
  RAge: number = 55;
  Remaining_Years: number = 30;

  // Retirement & Investment -----------


  ngOnInit() {
    // this.done.push({
    //   title: 'Mother',
    //   image: 'assets/img/wife.png'
    // });
   // this.Asys_login_post();
  //this.AAA_login();
    // this.family.splice(1,1);

    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      thirdCtrl: ['', Validators.required]
    });
    this.forthFormGroup = this._formBuilder.group({
      forthCtrl: ['', Validators.required]
    });
    this.fifthFormGroup = this._formBuilder.group({
      fifthCtrl: ['', Validators.required]
    });

    this.category = this.route
      .queryParams
      .subscribe(params => {
        this.categoryType = params;
      })
    console.log(this.categoryType.categoryNumber);

    switch (this.categoryType.categoryNumber) {
      case 'one':
        this.requestedMembers(this.Familyfather);
        this.father = true;
        break;
      case 'two':
        this.requestedMembers(this.Familyfather);
        this.requestedMembers(this.Familymother);
        this.father = true;
        this.mother = true;
        break;
      case 'three':
        this.requestedMembers(this.Familyfather);
        this.requestedMembers(this.Familymother);
        this.requestedMembers(this.Familyboy01);
        this.father = true;
        this.mother = true;
        this.boy01 = true;
        break;
      case 'four':
        this.requestedMembers(this.Familyfather);
        this.requestedMembers(this.Familymother);
        this.father = true;
        this.mother = true;
        break;
      default:
        break;
    }

  }

  Asys_login_post() {
    console.log("damith ");

    this.apiService.getAusysToken().subscribe(data => {
      console.log(data)
    });
  }

  //=============== Customer Details ===========

  requestedMembers(member: Member) {
    this.done.push(member);
    var index = this.family.findIndex(familyMember => familyMember === member);
    console.log(index);
    this.family.splice(index, 1);
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);

      console.log(event.container.data.length);
      console.log("container : "+event.container.id);
      console.log(JSON.parse(JSON.stringify(event.container.data[event.currentIndex])).title);

      if (event.container.id == "cdk-01") {
        var Selecttype = JSON.parse(JSON.stringify(event.container.data[event.currentIndex])).title;
        if (Selecttype === "Father") {
          this.father = true;
        }
        if (Selecttype === "Mother") {
          this.mother = true;
        }
        if (Selecttype === "Boy 01") {
          this.boy01 = true;
        }
        if (Selecttype === "Girl 01") {
          this.girl01 = true;
        }
      } else if (event.container.id == "cdk-02") {
        var Selecttype = JSON.parse(JSON.stringify(event.container.data[event.currentIndex])).title;
        if (Selecttype === "Father") {
          this.father = false;
        }
        if (Selecttype === "Mother") {
          this.mother = false;
        }
        if (Selecttype === "Boy 01") {
          this.boy01 = false;
        }
        if (Selecttype === "Girl 01") {
          this.girl01 = false;
        }
      }
    }



  }
  formatLabel(value: number) {
    return Math.round(value * 10000);
  }

  SalaryLabel(value: number) {
    if (value >= 1000) {
      return Math.round(value / 1000) + 'k';
    }
    return value;
  }

  changeIncome(event, type) {
    if (type == "F") {
      this.amount_father = Math.round(event.value / 1000);
    }
    if (type == "M") {
      this.amount_mother = Math.round(event.value / 1000);
    }

  }
  OnChangeRent(event) {
    console.log(event.checked);
    this.rentOption = event.checked;
  }
  changeAge(event, type) {
    if (type == "F") {
      this.age_father = event.value;
      this.Age = this.age_father;
    }
    if (type == "M") {
      this.age_mother = event.value;
    }
    if (type == "B1") {
      this.age_boy01 = event.value;
    }
    if (type == "G1") {
      this.age_girl01 = event.value;
    }
  }

  changeHotel(event) {
    this.hotel_expenses = Math.round(event.value / 1000);
  }

  changeCritical_Illness(event) {
    this.Critical_Illness = Math.round(event.value / 1000);
  }
  changeDisability_expenses(event) {
    this.Disability_expenses = Math.round(event.value / 1000);
  }
  changeRetirement_fund(event) {
    this.Retirement_fund = Math.round(event.value / 1000);
  }
  changeLife_Insurance(event) {
    this.Life_Insurance = Math.round(event.value / 1000);
  }
  //=============== END Customer Details ===========

  //=============== Financial Strength Details ===========

  changeCash_Other_assets(event) {
    this.Cash_Other_assets = Math.round(event.value / 1000);
    this.changeChartValues();
  }
  changeReal_Estate_Resident(event) {
    this.Real_Estate_Resident = Math.round(event.value / 1000);
    this.changeChartValues();
  }
  changeReal_Estate_No_Resident(event) {
    this.Real_Estate_No_Resident = Math.round(event.value / 1000);
    this.changeChartValues();
  }
  changeBusiness_Interest(event) {
    this.Business_Interest = Math.round(event.value / 1000);
    this.changeChartValues();
  }
  changePension_Fund(event) {
    this.Pension_Fund = Math.round(event.value / 1000);
    this.changeChartValues();
  }
  changeInsurance_cash_value(event) {
    this.Insurance_cash_value = Math.round(event.value / 1000);
    this.changeChartValues();
  }

  changeMortgage_Residence(event) {
    this.Mortgage_Residence = Math.round(event.value / 1000);
    this.changeChartValues();
  }
  changeMortgage_Others(event) {
    this.Mortgage_Others = Math.round(event.value / 1000);
    this.changeChartValues();
  }
  changeAuto_Loan(event) {
    this.Auto_Loan = Math.round(event.value / 1000);
    this.changeChartValues();
  }
  changeCredit_Card(event) {
    this.Credit_Card = Math.round(event.value / 1000);
    this.changeChartValues();
  }
  changeDebts(event) {
    this.Debts = Math.round(event.value / 1000);
    this.changeChartValues();
  }
  changeOther_debt_obligations(event) {
    this.Other_debt_obligations = Math.round(event.value / 1000);
    this.changeChartValues();
  }

  changeChartValues(): void {
    this.MFS_Assets = this.Cash_Other_assets +
      this.Real_Estate_Resident +
      this.Real_Estate_No_Resident +
      this.Business_Interest +
      this.Pension_Fund +
      this.Insurance_cash_value;

    this.MFS_Liabilities = this.Mortgage_Residence +
      this.Mortgage_Others +
      this.Auto_Loan +
      this.Credit_Card +
      this.Debts +
      this.Other_debt_obligations;


    this.MFS_Balance = this.MFS_Assets - this.MFS_Liabilities;
    // let data = [this.MFS_Assets, this.MFS_Liabilities];
    console.log(this.MFS_Assets);
    console.log(this.MFS_Liabilities);


    console.log(this.barChartData[0].data);
    this.barChartData[0].data = [this.MFS_Assets, this.MFS_Liabilities];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    // this.barChartData[0].data = data;
    this.barChartData = clone;


  }

  //Cash & Income Need ++++++++++
  CIN_init() {
    this.Immediate_Money_Fund = this.amount_father * 6;
    this.Debt_liquidation = this.amount_father * 6;
    this.Emergency_Fund = this.amount_father * 3;
    this.Educational_Fund = 10;
    this.changeCashNeed();
    this.changeIncomeNeed();
    this.changeProtectFamilyFinancial();
    console.log(this.Immediate_Money_Fund);

  }
  changeImmediate_Money_Fund(event) {
    this.Immediate_Money_Fund_times = event.value;
    this.Immediate_Money_Fund = this.amount_father * event.value;
    this.changeIncomeNeed();
    this.changeCashNeed();
  }
  changeDebt_liquidation(event) {
    this.Debt_liquidation_times = event.value;
    this.Debt_liquidation = this.amount_father * event.value;
    this.changeIncomeNeed();
    this.changeCashNeed();
  }
  changeEmergency_Fund(event) {
    this.Emergency_Fund_times = event.value;
    this.Emergency_Fund = this.amount_father * event.value;
    this.changeIncomeNeed();
    this.changeCashNeed();
  }
  changeEducational_Fund(event) {
    this.Educational_Fund = Math.round(event.value / 1000000);
    this.changeIncomeNeed();
    this.changeCashNeed();
  }

  OnChangeRentAmoutn(value) {
    console.log(value);
    this.CD_rent_amount = value;
    this.changeIncomeNeed();
    this.changeCashNeed();
  }

  changeIncome_Objective(event) {
    this.IncomeObjective = event.value;
    console.log(event.value);
    this.changeIncomeNeed();
    this.changeCashNeed();

  }
  OnChangeAnnualGuaranteedincomes(value) {
    console.log(value);
    this.AnnualGuaranteedincomes = value
    this.changeIncomeNeed();
    this.changeCashNeed();
  }
  changeAssumeInterest(event) {
    this.AssumeInterest = event.value;
    this.changeIncomeNeed();
    this.changeCashNeed();
  }
  OnChangeGuaranteedOtherIncomeAmount(value) {
    this.OtherAnnualincomes = value;
    this.changeIncomeNeed();
    this.changeCashNeed();
  }
  changeIncomeNeed() {
    console.log(this.amount_father);
    console.log(this.amount_mother);
    console.log(this.IncomeObjective);
    console.log(this.AnnualGuaranteedincomes);
    console.log(this.OtherAnnualincomes);
    console.log(this.AssumeInterest);

    var IncomeObjectiveTemp = (((this.amount_father + this.amount_mother) * 12000) * (this.IncomeObjective / 100));
    var IncomeShortageTEMP = (this.AnnualGuaranteedincomes + this.OtherAnnualincomes);
    var IncomeShortage = 0;
    if (IncomeObjectiveTemp > IncomeShortageTEMP) {
      IncomeShortage = (IncomeObjectiveTemp - IncomeShortageTEMP);
    } else if (IncomeObjectiveTemp < IncomeShortageTEMP) {
      IncomeShortage = (IncomeShortageTEMP - IncomeObjectiveTemp);
    } else {
      IncomeShortage = 0;
    }


    console.log("INCOME : " + IncomeShortage);

    this.IncomeNeedTemp = IncomeShortage * (100 / this.AssumeInterest);
    this.IncomeNeedString = formatCurrency(this.IncomeNeedTemp, 'en-US', 'Rs. ', 'LKR');
    this.changeProtectFamilyFinancial();
    console.log(this.IncomeNeedString);

  }

  changeCashNeed() {
    this.Cashneed = ((this.Immediate_Money_Fund + this.Debt_liquidation + this.Emergency_Fund + (this.Educational_Fund * 1000)) * 1000 + (this.rentOption ? (this.CD_rent_amount * 120) : (this.Mortgage_Residence * 1000))) - ((this.Cash_Other_assets + this.Life_Insurance) * 1000)
    this.CashNeedString = formatCurrency(this.Cashneed, 'en-US', 'RS. ', 'LKR');
    console.log(this.CashNeedString);
    this.changeProtectFamilyFinancial();
  }

  changeProtectFamilyFinancial() {
    this.ProtectFamilyFinancial = this.IncomeNeedTemp + this.Cashneed;
    this.ProtectFamilyFinancialString = formatCurrency(this.ProtectFamilyFinancial, 'en-US', 'RS. ', 'LKR');
  }
  OnChangeFocusAnnualIncomeatRetirement(value) {
    this.FocusAnnualIncomeatRetirement = value
    this.changeCapitalneeded();
  }

  changeAssumeinterestrate(event) {
    this.Assumeinterestrate = event.value;
    this.changeCapitalneeded();
  }
  changeCapitalneeded() {
    this.Capitalneeded = this.FocusAnnualIncomeatRetirement * (100 / this.Assumeinterestrate)
    this.CapitalneededString = formatCurrency(this.Capitalneeded, 'en-US', 'RS. ', 'LKR');

  }

  changeChildreneducationfund(event) {
    this.Childreneducationfund = event.value;
    this.ChildreneducationfundString = formatCurrency(this.Childreneducationfund, 'en-US', 'Rs. ', 'LKR');
  }

  changeOtherSavings(event) {
    this.OtherSavings = event.value;
    this.OtherSavingsString = formatCurrency(this.OtherSavings, 'en-US', 'Rs. ', 'LKR');
  }
  changeInvestmentLumpSum(event) {
    this.InvestmentLumpSum = event.value;
    this.InvestmentLumpSumString = formatCurrency(this.InvestmentLumpSum, 'en-US', 'Rs. ', 'LKR');
  }
  changeRAge(event) {

    this.RAge = event.value;
    this.Remaining_Years = this.RAge - this.Age;

  }
  CD_init() { }
}
