import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerComponent } from './customer/customer.component';
import { NewsComponent } from "./news/news.component";
import { CustomerCategoriesComponent } from "./customer-categories/customer-categories.component";


const routes: Routes = [
  { path: '', component: CustomerCategoriesComponent},
  { path: 'customer', component: CustomerComponent},
  { path: 'news', component: NewsComponent},
  { path: 'customerCategories', component: CustomerCategoriesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
